package com.wang.dmp.utils

import org.apache.commons.lang3.StringUtils
import org.apache.spark.sql.Row


object TagsHandler {

  //15个字段ID至少有一个不为空
  val hasNeedOneUserId =
    """
      |imei!='' or idfa!='' or mac!='' or androidid!='' or openudid!='' or
      |imeimd5!='' or idfamd5!='' or macmd5!='' or androididmd5!='' or openudidmd5!='' or
      |imeisha1!='' or idfasha1!='' or macsha1!='' or androididsha1!='' or openudidsha1!=''
    """.stripMargin //格式化这是

  //从一行数据中获取用户的某一个不为空的ID
  def getAnyOneUserId(row: Row) = {
    /* val imei = row.getAs[String]("imei")
    val idfa = row.getAs[String]("idfa")
    val mac  = row.getAs[String]("mac")
    val adId = row.getAs[String]("androidid")
    val udId = row.getAs[String]("openudid")

    val imeiMd5 = row.getAs[String]("imeimd5")
    val idfaMd5 = row.getAs[String]("idfamd5")
    val macMd5  = row.getAs[String]("macmd5")
    val adIdMd5 = row.getAs[String]("androididmd5")
    val udIdMd5 = row.getAs[String]("openudidmd5")

    val imeiSha1 = row.getAs[String]("imeisha1")
    val idfaSha1 = row.getAs[String]("idfasha1")
    val macSha1  = row.getAs[String]("macsha1")
    val adIdSha1 = row.getAs[String]("androididsha1")
    val udIdSha1 = row.getAs[String]("openudidsha1") */


    //判断是否字段有空  利用模式匹配
    row match {
      case v if StringUtils.isNotEmpty(v.getAs[String]("imei")) => "IM:" + v.getAs[String]("imei")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfa")) => "ID:" + v.getAs[String]("idfa")
      case v if StringUtils.isNotEmpty(v.getAs[String]("mac")) => "MC:" + v.getAs[String]("mac")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androidid")) => "AD:" + v.getAs[String]("androidid")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudid")) => "OU:" + v.getAs[String]("openudid")

      case v if StringUtils.isNotEmpty(v.getAs[String]("imeimd5")) => "IMM:" + v.getAs[String]("imeimd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfamd5")) => "IDM:" + v.getAs[String]("idfamd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("macmd5")) => "MCM:" + v.getAs[String]("macmd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androididmd5")) => "ADM:" + v.getAs[String]("androididmd5")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudidmd5")) => "OUM:" + v.getAs[String]("openudidmd5")

      case v if StringUtils.isNotEmpty(v.getAs[String]("imeisha1")) => "IMS:" + v.getAs[String]("imeisha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("idfasha1")) => "IDS:" + v.getAs[String]("idfasha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("macsha1")) => "MCS:" + v.getAs[String]("macsha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("androididsha1")) => "ADS:" + v.getAs[String]("androididsha1")
      case v if StringUtils.isNotEmpty(v.getAs[String]("openudidsha1")) => "OUS:" + v.getAs[String]("openudidsha1")
    }
  }

  /**
   * 获取当前行的所有的表示ID
   *
   * @param v
   */

  def getCurrentRowAllUserId(v: Row) = {

    var list = List[String]()
    if (StringUtils.isNotEmpty(v.getAs[String]("imei"))) list :+= "IM:" + v.getAs[String]("imei")
    if (StringUtils.isNotEmpty(v.getAs[String]("idfa"))) list :+= "ID:" + v.getAs[String]("idfa")
    if (StringUtils.isNotEmpty(v.getAs[String]("mac"))) list :+= "MC:" + v.getAs[String]("mac")
    if (StringUtils.isNotEmpty(v.getAs[String]("androidid"))) list :+= "AD:" + v.getAs[String]("androidid")
    if (StringUtils.isNotEmpty(v.getAs[String]("openudid"))) list :+= "OU:" + v.getAs[String]("openudid")
    if (StringUtils.isNotEmpty(v.getAs[String]("imeimd5"))) list :+= "IMM:" + v.getAs[String]("imeimd5")
    if (StringUtils.isNotEmpty(v.getAs[String]("idfamd5"))) list :+= "IDM:" + v.getAs[String]("idfamd5")
    if (StringUtils.isNotEmpty(v.getAs[String]("macmd5"))) list :+= "MCM:" + v.getAs[String]("macmd5")
    if (StringUtils.isNotEmpty(v.getAs[String]("androididmd5"))) list :+= "ADM:" + v.getAs[String]("androididmd5")
    if (StringUtils.isNotEmpty(v.getAs[String]("openudidmd5"))) list :+= "OUM:" + v.getAs[String]("openudidmd5")
    if (StringUtils.isNotEmpty(v.getAs[String]("imeisha1"))) list :+= "IMS:" + v.getAs[String]("imeisha1")
    if (StringUtils.isNotEmpty(v.getAs[String]("idfasha1"))) list :+= "IDS:" + v.getAs[String]("idfasha1")
    if (StringUtils.isNotEmpty(v.getAs[String]("macsha1"))) list :+= "MCS:" + v.getAs[String]("macsha1")
    if (StringUtils.isNotEmpty(v.getAs[String]("androididsha1"))) list :+= "ADS:" + v.getAs[String]("androididsha1")
    if (StringUtils.isNotEmpty(v.getAs[String]("openudidsha1"))) list :+= "OUS:" + v.getAs[String]("openudidsha1")
    list
  }
}
